package tests;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class ParseTests extends AbstractTestSuite { 
    
	public ParseTests() {
		super("testfiles/parser"); // directory to test files
	}
    
    @Test
	public void testProduction1() {
		testValidSyntax("testProduction1.newline");
	}
    
    @Test
	public void testproduction2() {
		testValidSyntax("testProduction2.newline");
	}
    
    @Test
	public void testproductions1() {
		testValidSyntax("testProductions1.newline");
	}
	
	@Test
	public void testproductions2() {
		testValidSyntax("testProductions2.newline");
	}
	
	@Test
	public void testproductions3() {
		testValidSyntax("testProductions3.newline");
	}
    
    @Test
	public void testTNT1() {
		testValidSyntax("testTNT1.newline");
	}
	
	@Test
	public void testoptionline1() {
		testValidSyntax("testOptionLine1.newline");
	}
	
	@Test
	public void testblock1() {
		testValidSyntax("testBlock1.newline");
	}
	
	@Test
	public void testblock2() {
		testValidSyntax("testBlock2.newline");
	}
	
	@Test
	public void testblock3() {
		testValidSyntax("testBlock3.newline");
	}
	
	@Test
	public void testterminals1() {
		testValidSyntax("testTerminals1.newline");
	}
	
	@Test
	public void testterminals2() {
		testValidSyntax("testTerminals2.newline");
	}
	
	@Test
	public void testifstmt() {
		testValidSyntax("testIfstmt.newline");
	}
    
    @Test
    public void errorSEP1() {
		testSyntaxError("errorSEP1.newline");
	}
    
    @Test
    public void errorproduction1() {
		testSyntaxError("errorProduction1.newline");
	}
	
	@Test
    public void errorblock1() {
		testSyntaxError("errorBlock1.newline");
	}
	
	@Test
    public void errorterminals1() {
		testSyntaxError("errorTerminals1.newline");
	}
	
}
package lang;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.ArrayDeque;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;
import lang.ast.LangScanner;

import beaver.*;

public class LangPreprocessor extends beaver.Scanner {
	
	static public boolean manualcheck = false; // enable standardprintouts for manual checking...
	
	private TokenStream ts; // token stream
	
    static public int tabSize = 4; // in order to get correct column-counts
	
	private int globalCounter = 0; // global counter for nextToken-method
	
	// block indentation options
	static public final short LESS = 0;
	static public final short LEQ = 1;
	static public final short EQUAL = 2;
	static public final short MORE = 3;
	static public final short MEQ = 4;
	static public final short EXACT = 5;
    
    // Constructor - give it the entire token stream
	public LangPreprocessor (LangScanner scanner) {
		
		// create token stream object
		ts = new TokenStream (scanner);
		
        // Print unedited tokenstream
        if(manualcheck) {
            for(LPSymbol s: ts.tokens) {
                System.out.println("|"+Terminals.NAMES[s.getId()]+"| - l:"+s.getLine()+" c:"+s.getColumn());
            }
			
            System.out.println("_________");
			System.out.println("");
		}
		
		// Correct column-numbers and delete tabs
		ts.columnCorrect();
		
		// Delete extra NEWLINE tokens  
		ts.newlineCorrect();
		
		// Call function that inserts needed INDENT and DEDENT tokens
		editTokenStream();
	}
	
/* functions that manage editing of the token stream */
    
	public void editTokenStream() {
		tsloop:
		while (ts.pos < ts.tokens.size()) {
			if(manualcheck){System.out.println("i = "+ts.pos+" with Token |"+Terminals.NAMES[ts.tokens.get(ts.pos).getId()]+"|");}
			
			switch(ts.tokens.get(ts.pos).getId()) {
				case Terminals.EOF:
                    ts.addDEDENT(Terminals.EOF);
					break tsloop;
				case Terminals.NONTERMINAL:
					// a nonterminal can end a continuation line
					ts.undoCon();
					
                    // a nonterminal can end a block
                    ts.checkUnBlock();
                    
                    // check for level of indentation inside "special block"
                    if(ts.checkLevel(MORE)) {ts.doLevel(MORE);}
                    else if(ts.checkLevel(EQUAL)) {ts.doLevel(EQUAL);}
                    else {ts.reportError();}
                    
                    // a nonterminal can be a reference/trigger for a block, but only if an ASSIGN-token follows 
                    // meaning at the very beginning of a production
					// it can also be the reference point for a continuation
                    if(ts.tokens.get(ts.pos+1).getId() == Terminals.ASSIGN) {
                        ts.doBlockTrigger(MORE);
                    }
                    
                    // a nonterminal can also start a regular block
                    ts.checkBlock();
                    break;
				case Terminals.NEWLINE:
					// If in a special block, set the boolean to true, that determines whether a 
					// token is the first in its line
                    if(ts.inSpecialBlock) {
                        ts.firstInLine = true;
                    }
                    break;
				case Terminals.REF:
                    // the REF-token starts the "special block" - a special block is a block where the level 
                    // of indentation has to be checked multiple times for different lines and values... 
                    ts.doSpecialBlockTrigger(MEQ);
					ts.firstInLine = false;
                    break;
				case Terminals.SEP:
					ts.doConTrigger();
					ts.checkCon(MEQ,1);
					break;
				case Terminals.SPACES: break;
				case Terminals.PLUS: break;
				case Terminals.KLEEN: break;
				case Terminals.QMARK: break;
				case Terminals.CONT: break;
				case Terminals.ASSIGN: 
					ts.doConReference(1);
                    ts.checkBlock(false);
                    break;
				case Terminals.DEDENT: break;
				case Terminals.TAB: break;
				case Terminals.EQ: 
                    ts.doLevel(EQUAL);
                    break;
				case Terminals.MEQ:              
                    ts.doLevel(MEQ);
                    break;
				case Terminals.INDENT: break;
				case Terminals.TERMINAL: 
                    ts.checkUnBlock();
                    
                    if(ts.checkLevel(MORE)) {ts.doLevel(MORE);}
                    else if(ts.checkLevel(EQUAL)) {ts.doLevel(EQUAL);}
                    else {ts.reportError();}
                    
                    ts.checkBlock();
                    break;
				default: break;
			}
			ts.pos++;
		}
		
		// Print out tokens
        if(manualcheck) {
			System.out.println("_________");
			System.out.println("");
            for(int j=0; j < ts.tokens.size(); j++) {
                System.out.println("|"+Terminals.NAMES[ts.tokens.get(j).getId()]+"| - l:"+ts.tokens.get(j).getLine()+" c:"+ts.tokens.get(j).getColumn());
            }
        }
	}

/* connector to beaver parser */
	
	public beaver.Symbol nextToken() throws java.io.IOException {
		// simple function that beaver uses to get the next token
		globalCounter++;
		return new beaver.Symbol (  ts.tokens.get(globalCounter-1).getId(), 
                                    ts.tokens.get(globalCounter-1).getLine(),
                                    ts.tokens.get(globalCounter-1).getColumn(),
                                    ts.tokens.get(globalCounter-1).getLength(),
                                    ts.tokens.get(globalCounter-1).getValue()    );
	}	
}
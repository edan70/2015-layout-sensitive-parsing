package lang;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.ArrayDeque;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;
import lang.ast.LangScanner;

import beaver.*;

public class TokenStream {
	public ArrayList<LPSymbol> tokens = new ArrayList<LPSymbol>();
	
	public int pos = 0; // global counter for all methods that play with the token steam
	private boolean indentable = false;
    public boolean inSpecialBlock = false;
    public boolean firstInLine = false;
	private boolean continuable = false;
	private Deque<Integer> blockColStack = new ArrayDeque<Integer>();
    private Deque<Integer> blockLineStack = new ArrayDeque<Integer>();
    private Deque<Short> indentTypeStack = new ArrayDeque<Short>();
	private int conColRef;
	
	public TokenStream(LangScanner scanner) {
		try {
			tokens.add(scanner.nextToken());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Get rest of symbols into tokenStream assuming EOF token exists
		while ((tokens.get(tokens.size()-1)).getId()!=Terminals.EOF) {
			try {
				tokens.add(scanner.nextToken());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
/* methods for correction of columns if mixing of tabs and spaces occurs */

	// If a tab is seen, add appropriate amount of columns to the appropriate following tokens
	// and remove the tab token!
	public void columnCorrect() {
		while (pos < tokens.size()) {
			switch (tokens.get(pos).getId()) {	
				case Terminals.TAB:
                    int colCount = tokens.get(pos).getColumn()+1;
                    // find out at what position the next tabstopp is
                    while ((colCount-1)%LangPreprocessor.tabSize!=0){
                        colCount++;
                    }
                    
                    // calculate how many spaces have to be added to following tokens
                    int toAdd = colCount - tokens.get(pos).getColumn() - 1;
                    
					addCols(toAdd);
					removeTab();
					break;
			}
			pos++;
		}
		pos = 0;
	}
	
	// add columns until a new line is started or EOF-tokens is seen
	public void addCols(int toAdd) {
		int index = pos+1;
        while (tokens.get(pos).getLine() == tokens.get(index).getLine() && tokens.get(index).getId() != Terminals.EOF) {
            tokens.get(index).setColumn(tokens.get(index).getColumn()+toAdd);
			index++;
        }
    }
	
	public void removeTab() {
		tokens.remove(pos);
		pos--;
	}


/* methods for handling continuation */
	
	// method that should be inserted in case a token can be trigger continuation line
	public void doConTrigger() {
		if(!continuable){	
			continuable = true;
		}
	}
	
	// method that should be inserted in case a token can be a reference point for a 
	// continuation line
	public void doConReference() {doConReference(pos);}
	
	public void doConReference(int offset) {
		int position = pos + offset;
		conColRef = tokens.get(position).getColumn();
	}
	
    // check whether there is a continuation in progress 
	public void checkCon(short indentType) {
		try {
			checkCon(indentType,0,0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    
	public void checkCon(short indentType, int offset) {
		try {
			checkCon(indentType,0,offset);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void checkCon(short indentType, int spaceNo, int offset) throws IOException {
		int position = pos + offset;
		if(position+1 >= tokens.size()) {
			return;
		}
		if(	// general conditions
			continuable &&
			tokens.get(position+1).getLine() > tokens.get(position).getLine() &&
			
			// conditions for different indentTypes
			((indentType == LangPreprocessor.LESS && tokens.get(position+1).getColumn() < conColRef) ||
			(indentType == LangPreprocessor.LEQ && tokens.get(position+1).getColumn() <= conColRef) ||
			(indentType == LangPreprocessor.EQUAL && tokens.get(position+1).getColumn() == conColRef) ||
			(indentType == LangPreprocessor.MORE && tokens.get(position+1).getColumn() > conColRef) ||
			(indentType == LangPreprocessor.MEQ && tokens.get(position+1).getColumn() >= conColRef) ||
			(indentType == LangPreprocessor.EXACT && tokens.get(position+1).getColumn() == conColRef+spaceNo)))
		{
			int index = position+1;
			int onLine = tokens.get(position+1).getLine();
			int deltaLine = tokens.get(position).getLine()-onLine;
			int deltaCol = tokens.get(position).getColumn()-onLine;
			// while tokens are on the same line as the token following the continuation trigger
			// pull tokens up to the line of the continuation trigger and offset the column appropriately
			// deltaLine is used for multiple lines of continuation. Should first line be pulled up to 
			// original line, next continuation line needs to be pulled up 2 lines!
				while (tokens.get(index).getLine() == onLine){
					tokens.get(index).setLine(tokens.get(index).getLine()+deltaLine);
					tokens.get(index).setColumn(tokens.get(index).getColumn()+deltaCol);
					index++;
					// to solve end of file problems (array out of bounds) check for invalid index
					// !! could be solved more elegantly with exceptions i'm sure... !!
					if (index >= tokens.size()) {break;}
				}
			// delete NEWLINE-token
			tokens.remove(pos+1);
		// need else if clause in which nothing is done to easier be able to use the else clause...
		} else if(tokens.get(pos+1).getLine() == tokens.get(pos).getLine()) {
		} else {
			throw new IOException("Invalid end of line at line "+tokens.get(pos).getLine()+". Probable cause: illegal indentation after attempted continuation of line.");
		}
	}
	
	// method "detriggers" continuation line, should be inserted at tokens that start a new statement for example
	public void undoCon() {
		if(pos-1 > 0) {
			if(tokens.get(pos).getLine() > tokens.get(pos-1).getLine()) {
				continuable = false;
			}
		}
	}
	
/* mothods for handling blocks */
	
	// method should be inserted whenever a token is seen that is a reference point for a following
	// block element (such as an IF-token)
	public void doBlockTrigger(short indentType) {
		indentable = true;
		blockColStack.addLast(tokens.get(pos).getColumn());
        blockLineStack.addLast(tokens.get(pos).getLine());
        indentTypeStack.addLast(indentType);
	}
    
    public void doSpecialBlockTrigger(short indentType) {
        inSpecialBlock = true;
        doBlockTrigger(indentType);
    }
	
	// checks whether a block is following
	// should be inserted for all tokens that can be followed by a block structure
	public void checkBlock() {
		checkBlock(0,true);
	}
    
    public void checkBlock(boolean mustNewline) {
        checkBlock(0,mustNewline);
    }
	
	public void checkBlock(int spaceNo, boolean mustNewline) {
        int position = pos;
        if(tokens.get(pos+1).getId() == Terminals.NEWLINE) {
            position++;
        }
		
		// ADD SOME SORT OF EXCEPTION WITH ERROR MESSAGE
		if(!(tokens.get(pos+1).getId() == Terminals.NEWLINE) && mustNewline){
			return;
		}
        
        if(	// general conditions
			indentable &&
            ((mustNewline && tokens.get(position+1).getLine() > tokens.get(pos).getLine()) ||
            (!mustNewline)) && 
			tokens.get(position+1).getId() != Terminals.EOF &&
			blockColStack.size() > 0 &&
			
			// conditions for different indentTypes
			((indentTypeStack.getLast() == LangPreprocessor.LESS && tokens.get(position+1).getColumn() < blockColStack.getLast()) ||
			(indentTypeStack.getLast() == LangPreprocessor.LEQ && tokens.get(position+1).getColumn() <= blockColStack.getLast()) ||
			(indentTypeStack.getLast() == LangPreprocessor.EQUAL && tokens.get(position+1).getColumn() == blockColStack.getLast()) ||
			(indentTypeStack.getLast() == LangPreprocessor.MORE && tokens.get(position+1).getColumn() > blockColStack.getLast()) ||
			(indentTypeStack.getLast() == LangPreprocessor.MEQ && tokens.get(position+1).getColumn() >= blockColStack.getLast()) ||
			(indentTypeStack.getLast() == LangPreprocessor.EXACT && tokens.get(position+1).getColumn() == blockColStack.getLast()+spaceNo))) 
		{
			addINDENT(position+1);
		}
	}
	
	// checks whether the token ends the block structure
    // should be inserted whenever a token is the start of something AFTER
    // a block structure is finished
	public void checkUnBlock() {
		checkUnBlock(0);
	}
	
	public void checkUnBlock(int spaceNo) {
		while (	
			// general conditions
			pos-1 >= 0 &&
			tokens.get(pos).getLine() > tokens.get(pos-1).getLine() && 
			tokens.get(pos).getId() != Terminals.EOF &&
			blockColStack.size() > 0 &&
			
			// conditions for different indentTypes
			((indentTypeStack.getLast() == LangPreprocessor.LESS && tokens.get(pos).getColumn() >= blockColStack.getLast()) ||
			(indentTypeStack.getLast() == LangPreprocessor.LEQ && tokens.get(pos).getColumn() > blockColStack.getLast()) ||
			(indentTypeStack.getLast() == LangPreprocessor.EQUAL && tokens.get(pos).getColumn() != blockColStack.getLast()) ||
			(indentTypeStack.getLast() == LangPreprocessor.MORE && tokens.get(pos).getColumn() <= blockColStack.getLast()) ||
			(indentTypeStack.getLast() == LangPreprocessor.MEQ && tokens.get(pos).getColumn() < blockColStack.getLast()) ||
			(indentTypeStack.getLast() == LangPreprocessor.EXACT && tokens.get(pos).getColumn() != blockColStack.getLast()+spaceNo))) 
		{
            // BECAUSE WE'RE USING NEWLINE GENERATED BY THE SCANNER DEDENT NEEDS TO BE INSERTED
            // AT pos-1!
            inSpecialBlock = false;
			addDEDENT(pos-1);
		}
	}
    
    // check indentation level without the need for actually adding INDENT- and DEDENT-tokens
    // NEED FOR PROPER ERROR MESSAGING!
    public boolean checkLevel(short indentType) {
        return checkLevel(indentType,pos,0);
    }
    
    public boolean checkLevel(short indentType, int position, int offset) {
		int toCheck = position + offset;
        if (inSpecialBlock && firstInLine) {
            if (indentType == LangPreprocessor.EQUAL && tokens.get(toCheck).getColumn() == blockColStack.getLast()) {
                return true;
            } else if (indentType == LangPreprocessor.MORE && tokens.get(toCheck).getColumn() > blockColStack.getLast()) {
                return true;
            } else if (indentType == LangPreprocessor.MEQ && tokens.get(toCheck).getColumn() == blockColStack.getLast()) {
                return true;
            } else if (indentType == LangPreprocessor.EXACT && tokens.get(toCheck).getColumn() == blockColStack.getLast()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
    
    // actually edit token stream by doing necessary action
    public void doLevel(short indentType) {
        doLevel(indentType,pos,0);
    }
    
    public void doLevel(short indentType, int position, int offset) {
		int toCheck = position + offset;
        if (inSpecialBlock &&  firstInLine) {
            if (indentType == LangPreprocessor.EQUAL && tokens.get(toCheck).getColumn() == blockColStack.getLast()) {
                firstInLine = false;
            } else if (indentType == LangPreprocessor.MORE && tokens.get(toCheck).getColumn() > blockColStack.getLast()) {
                tokens.add(toCheck, new LPSymbol(Terminals.MORE, tokens.get(toCheck).getLine(), tokens.get(toCheck).getColumn(), 0));
                if(position <= pos) {
                    pos++;
                }
                firstInLine = false;
            } else if (indentType == LangPreprocessor.MEQ && tokens.get(toCheck).getColumn() == blockColStack.getLast()) {
                firstInLine = false;
            } else if (indentType == LangPreprocessor.EXACT && tokens.get(toCheck).getColumn() == blockColStack.getLast()) {
                firstInLine = false;
            } else {
            }
        }
    }
	
/* methods handling newlines - mostly necessary when NEWLINE token is not given by the scanner but needs to be inserted */
	
	// Check whether a NEWLINE token should be inserted, used at tokens that are allowed to start a new line
	public void checkNewline() {
		if(	pos-1 >= 0  && 
			tokens.get(pos).getLine() > tokens.get(pos-1).getLine() &&
			tokens.get(pos-1).getId() != Terminals.INDENT			){
			addNEWLINE();
		}
	}
	
	// Actually quite similar to checkNewline just that standard version checks whether the next token is on 
	// a line with a bigger line count than current token.
	// Also this function adds a NEWLINE token in order to produce a parsing error.
	// Should be used on tokens that are not allowed to be followed by a NEWLINE!
	public void checkNoNewline() {
		checkNoNewline(1, 0);
	}
	
	public void checkNoNewline(int a, int b) {
		a+=pos;
		b+=pos;
		if(tokens.get(a).getLine() > tokens.get(b).getLine()) 
		{
			addNEWLINE(a);
			if(a <= pos) {
				pos++;
			}
		}
	}
	
	// get rid of all unnecessary NEWLINE tokens (i.e. if there are two newline tokens, delete one!)
	public void newlineCorrect() {
		int i = 0;
		while (i<tokens.size()){
			if (tokens.get(i).getId() == Terminals.NEWLINE &&
                (tokens.get(i+1).getId() == Terminals.NEWLINE || 
                tokens.get(i+1).getId() == Terminals.EOF)
                ) 
			{
				tokens.remove(i);
			} else {
                i++;
            }
		}
	}
	
/* methods for insertion of tokens */
	
	public void addINDENT() {
        addINDENT(pos+1);
    }
    
    public void addINDENT(int position) {
		if(LangPreprocessor.manualcheck){System.out.println("Responsible for INDENT: |"+Terminals.NAMES[tokens.get(position-1).getId()]+"| - l:"+tokens.get(position-1).getLine()+" c:"+tokens.get(position-1).getColumn());}
		tokens.add(position, new LPSymbol(Terminals.INDENT, tokens.get(position-1).getLine(), tokens.get(position-1).getColumn(), 0));
		indentable = false;
	}
	
	public void addDEDENT() {
        addDEDENT(pos);
    }
    
    public void addDEDENT(int position) {
		if(LangPreprocessor.manualcheck){
			System.out.println("Responsible for DEDENT: |"+Terminals.NAMES[tokens.get(position).getId()]+"| - l:"+tokens.get(position).getLine()+" c:"+tokens.get(position).getColumn());
			System.out.println("THIS TOKEN COL: "+tokens.get(position).getColumn());
			System.out.println("STACK COL: "+blockColStack.getLast());
		}
		tokens.add(position, new LPSymbol(Terminals.DEDENT, tokens.get(position-1).getLine(), tokens.get(position-1).getColumn(), 0));
		blockColStack.removeLast();
        blockLineStack.removeLast();
        indentTypeStack.removeLast();
        
        if(position <= pos) {
            pos++;
        }
	}
	
	// Special adding of DEDENT-tokens for the EOF
	public void addDEDENT(short special) {
		while (blockColStack.size() > 0) {
			tokens.add(pos, new LPSymbol(Terminals.DEDENT, tokens.get(pos-1).getLine(), tokens.get(pos-1).getColumn(), 0));
			blockColStack.removeLast();
            blockLineStack.removeLast();
            indentTypeStack.removeLast();
			pos++;
		}
	}
	
	public void addNEWLINE() {
		addNEWLINE(pos);
	}
	
	public void addNEWLINE(int position) {
		if(LangPreprocessor.manualcheck){System.out.println("Responsible for NEWLINE: |"+Terminals.NAMES[tokens.get(pos).getId()]+"| - l:"+tokens.get(pos).getLine()+" c:"+tokens.get(pos).getColumn());}
		tokens.add(position, new LPSymbol(Terminals.NEWLINE, tokens.get(position-1).getLine(), tokens.get(position-1).getColumn(), 0));
		
		if(position <= pos) {
			pos++;
		}
	}
    
    public void reportError() {
        System.err.println("There is an indentation issue near l: "+tokens.get(pos+1).getLine()+" c:"+tokens.get(pos+1).getColumn());
        tokens.add(pos, new LPSymbol(Terminals.EOF, tokens.get(pos-1).getLine(), tokens.get(pos-1).getColumn(), 0));
        pos++;
    }
}
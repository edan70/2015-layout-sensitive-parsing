package lang.ast;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;
import lang.LPSymbol;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the preprocessor is the nextToken() method
%type LPSymbol 
%function nextToken 

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  public LPSymbol sym(short id) {
    return new LPSymbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

// macros
WhiteSpace 	= [ ] | \f | \r // no tabs here
nonterminal = [a-z]+
terminal	= [A-Z]+
spaces      = _+

%%

// discard whitespace information
{WhiteSpace}  { }

// token definitions


"(=)"           { return sym(Terminals.EQ); }
//"(<=)"	    { return sym(Terminals.LEQ); }
"(>=)"			{ return sym(Terminals.MEQ); }
{spaces}      	{ return sym(Terminals.SPACES); }
"'"				{ return sym(Terminals.REF); }
"*"             { return sym(Terminals.KLEEN); }
"+"				{ return sym(Terminals.PLUS); }
"?"				{ return sym(Terminals.QMARK); }
"->"			{ return sym(Terminals.CONT); }
"="            	{ return sym(Terminals.ASSIGN); }
"|"				{ return sym(Terminals.SEP); }
"\n"			{ return sym(Terminals.NEWLINE); }
"\t"			{ return sym(Terminals.TAB); }
{nonterminal}	{ return sym(Terminals.NONTERMINAL); }
{terminal}		{ return sym(Terminals.TERMINAL); }
<<EOF>>         { return sym(Terminals.EOF); }

// error fallback
[^]           { throw new SyntaxError("Illegal character <"+yytext()+">"); }

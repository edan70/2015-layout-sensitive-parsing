# Layout-sensitive parsing using JFlex and Beaver

### Project structure
There are two main project parts in the following subdirectories:

1. NEWLINE   
2. Python-occam

Both of these subdirectories contain independant build-files. As the names suggest 1. contains a parser for the NEWLINE-language, a simple language that allows the specification of layout-sensitive grammars. 2. Contains parsers for subsets of Python and occam. Note that the NEWLINE-parser is the most advanced of the three parsers and uses the most current code-base. The parsers for the Python and occam subsets work but might be slightly outdated in terms of structure and methods - they could easily be rewritten using the code-base of the NEWLINE-parser.

Before running, the folder structure is as follows (NL: only in the NEWLINE-directory):
 
* .\lib	- contains all necessary library files, i.e.
	* JFlex (scanner-generator)
	* Beaver (parser-generator)
	* JUnit (testing environment)
	* NL: JastAdd (parse-tree generation)
	* Hamcrest (utility)
* .\src - contains the source files
	* NL: .\jastadd - contains JastAdd-grammar and -aspects
	* .\java
		* lang - contains class that holds main method and all preprocessor source files
		* tests - contains source files for test-environment
	* .\parser - contains Beaver-specification
	* .\scanner - contains JFlex-specification
* .\testfiles - contains testfiles for different tests

In the Python-occam-directory, the subdirectory .\java\lang has further subdirectory corresponding to the language and type being currently parsed (as specified in the build.properties file). While this folder structure is at odds with the package-declarations in the files, once the build file is run, the compiled Java-classfiles will be correctly filed according to the package-declaration.

When run, the following generated structure is added to the existing folder-structure:
   
* .\ant-bin - this is where all Java-class-files will be outputted to
* .src\gen\lang\ast - all the generated .java-files are saved here, i.e. by Beaver, JFlex and JastAdd
____

### How to run the test-cases

- Install the current distribution of Java on your machine https://java.com/en/download/ and make sure that it works
- Install Apache Ant: https://ant.apache.org/bindownload.cgi, follow install-instructions and make sure that it works
- Copy the project onto your machine
- To run entire test-suite simply run ANT through the command line in the root directory (containing the build.xml-file) using the following command:

	```
	ant test
	```

	This will run:    
		a) JFlex-scanner-generator,   
		b) Beaver-parser-generator,   
		c) JastAdd (for NEWLINE-parser) 
       	d) compile all necessary Java-files and  
		e) automatically run the test suite

	For the Python- and occam-parsers there is only a simple test that passes if the source code of all test files is syntactically valid. For these parsers it is not evident if the correct parse-tree has been built. If you want to see the result of the preprocessor, you can open the LangPreprocessor.java-file in .\src\java\lang\{langprefix}\{typeprefix} and change the value of the boolean manualcheck from false to true:
		
	```
	static public boolean manualcheck = true;
	```	 

	This will print the original token stream, some useful information and the preprocessors output token stream. Note that it will do this for all testfiles which might result in quite some output. To avoid this, check testfiles individually (next point).
	
	The NEWLINE-parser on the other hand produces and output file through the DumpTree-test with the file-extension .out. These output files are saved in the testfile-directory (.\testfiles\ast). In order for this test to pass, the expected output needs to be specified in a file in the testfile-directory with the file-extension .expected. These expected-output-files can also be produced after running (and failing) the test for the first time by copying the contents of the produced .out-files by the following command:

	```
	ant output2expected
	```

	Note that this should obviously only be done after the correctness of the .out-files has been checked.


- To run single testfiles (existing or of your own creation, do not have to be in testfile-directory) it is necessary to first create a java archive:

	```
	ant jar
	```

	This will run:    
		a) JFlex-scanner-generator,   
		b) Beaver-parser-generator,   
		c) JastAdd (for NEWLINE-parser)   
       	d) compile all necessary Java-files and  
		e) create a Java-archive.

	- Now the Java archive can be run through command line with the input file as an argument
		
		```
		java -jar compiler.jar .\testfiles\simple_test1.mypy
		```

- Use the following ant command delete generated directories and files and reset

	```
	ant clean
	```

- To switch between Python- and occam-parsers, edit the build.properties-file in the Python-occam-folder. Change languageprefix and typeprefix to one of the following options:   
		
	```	
	languageprefix   
		= python   
		= occam
	```

	```
	typeprefix [Python]
		= regnl - only regular newline, not working properly
		= specnl - if-else clause working (with special newline)
		= elif - added elif clause	
	```

	```
	typeprefix [occam]
		= simple
		= seperated	
	```

- There are several other ant-commands:
	- run only JFlex:
		```	
		ant scanner-gen
		```
	- run only Beaver:
		```	
		ant parser-gen
		```
	- run only JastAdd:
		```	
		ant ast-gen
		```
	- run all generation:
		```	
		ant gen
		```
	- run all generation and compile Java-files:
		```	
		ant build
		```



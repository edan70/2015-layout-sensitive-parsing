package tests;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class ParseTestspythonspecnl extends AbstractTestSuite { 
    
	public ParseTestspythonspecnl() {
		super("testfiles/python/specnl"); // directory to test files
	}
    
    @Test
	public void else1() {
		testValidSyntax("test_else1.python");
	}
    
    @Test
	public void else2() {
		testValidSyntax("test_else2.python");
	}
    
    @Test
	public void if1() {
		testValidSyntax("test_if1.python");
	}
    
    @Test
	public void if2() {
		testValidSyntax("test_if2.python");
	}
    
}
package tests;

import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.Scanner;

import lang.ast.LangParser;
import lang.ast.LangScanner;
import lang.LangPreprocessor;

abstract class AbstractTestSuite {
    protected final File testDirectory;

    public AbstractTestSuite(String testDirectory) {
        this.testDirectory = new File(testDirectory);
    }
    
    protected void testValidSyntax(String filename) {
        try {
            parse(new File(testDirectory, filename));
        } catch (Exception e) {
            fail("Unexpected error while parsing '"+filename+"': "+e.getMessage());
        }
    }
    
    protected void testSyntaxError(String filename) {
        try {
            // discard Beaver standard error
            System.setErr(new PrintStream(new ByteArrayOutputStream()));
            
            parse(new File(testDirectory, filename));
            
            fail("Syntax is valid, expected syntax error!");
        } catch (beaver.Parser.Exception | lang.ast.LangParser.SyntaxError e) {
        } catch (Exception e) {
            fail("IO error while trying to parse '"+filename+"': "+e.getMessage());
        }
    }
    
    protected static Object parse(File file) throws IOException, Exception {
        LangScanner scanner = new LangScanner(new FileReader(file));
        LangPreprocessor preprocessor = new LangPreprocessor(scanner);
		LangParser parser = new LangParser();
        
        return parser.parse(preprocessor);
    }
}
package tests;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class ParseTestspythonswitch extends AbstractTestSuite { 
    
	public ParseTestspythonswitch() {
		super("testfiles/python/switch"); // directory to test files
	}
    
    @Test
	public void elif1() {
		testValidSyntax("test_elif1.python");
	}
    
    @Test
	public void else1() {
		testValidSyntax("test_else1.python");
	}
    
    @Test
	public void else2() {
		testValidSyntax("test_else2.python");
	}
    
    @Test
	public void if1() {
		testValidSyntax("test_if1.python");
	}
    
    @Test
	public void if2() {
		testValidSyntax("test_if2.python");
	}
    
    @Test
	public void if3() {
		testValidSyntax("test_if3.python");
	}
    
	@Test
	public void tab1() {
		testValidSyntax("test_tab1.python");
	}
	
	@Test
	public void tab2() {
		testValidSyntax("test_tab2.python");
	}
	
    @Test
	public void error1() {
		testSyntaxError("test_error1.python");
	}
    
    @Test
    public void error2() {
		testSyntaxError("test_error2.python");
	}
    
    @Test 
    public void error3() {
		testSyntaxError("test_error3.python");
	}
    
    @Test
    public void error4() {
		testSyntaxError("test_error4.python");
	}
    
    @Test
    public void error5() {
		testSyntaxError("test_error5.python");
	}
}
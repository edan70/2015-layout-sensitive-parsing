package tests;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class ParseTestsoccamseperated extends AbstractTestSuite { 
    
	public ParseTestsoccamseperated() {
		super("testfiles/occam/seperated"); // directory to test files
	}
    
    @Test
	public void test1() {
		testValidSyntax("test1.occam");
	}
	
	@Test
	public void testSEQ1() {
		testValidSyntax("testSEQ1.occam");
	}
	
	@Test
	public void testSEQ2() {
		testValidSyntax("testSEQ2.occam");
	}
	
	@Test
	public void testSEQ3() {
		testValidSyntax("testSEQ3.occam");
	}
	
	@Test
	public void testSEQ4() {
		testValidSyntax("testSEQ4.occam");
	}
	
	@Test
	public void testSEQ5() {
		testValidSyntax("testSEQ5.occam");
	}
	
	@Test
	public void testRepli1() {
		testValidSyntax("testRepli1.occam");
	}
	
	@Test
	public void testRepli2() {
		testValidSyntax("testRepli2.occam");
	}
	
	@Test
	public void testRepli3() {
		testValidSyntax("testRepli3.occam");
	}
	
	@Test
	public void testTAB1() {
		testValidSyntax("testTAB1.occam");
	}
	
	@Test
	public void testTAB2() {
		testValidSyntax("testTAB2.occam");
	}
	
	@Test
	public void testProcess1() {
		testValidSyntax("testProcess1.occam");
	}
	
	@Test
	public void testOperand1() {
		testValidSyntax("testOperand1.occam");
	}
	
	@Test
	public void testOperand2() {
		testValidSyntax("testOperand2.occam");
	}
	
	@Test
	public void testPAR1() {
		testValidSyntax("testPAR1.occam");
	}
	
	@Test
	public void testCon1() {
		testValidSyntax("testCon1.occam");

	}
	
	@Test
	public void testCon2() {
		testValidSyntax("testCon2.occam");
	}
	
	@Test
	public void testCon3() {
		testValidSyntax("testCon3.occam");
	}
	
	@Test
	public void testCon4() {
		testValidSyntax("testCon4.occam");
	}
	
	@Test
	public void testCon5() {
		testValidSyntax("testCon5.occam");
	}
	
}
package lang;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.ArrayDeque;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;
import lang.ast.LangScanner;

import beaver.*;

public class TokenStream {
	public ArrayList<LPSymbol> tokens = new ArrayList<LPSymbol>();
	
	public int pos = 0; // global counter for all methods that play with the tokenSteam
	private boolean indentable = false;
	private boolean continuable = false;
	private Deque<Integer> blockColStack = new ArrayDeque<Integer>();
	private int conColRef;
	
	public TokenStream(LangScanner scanner) {
		try {
			tokens.add(scanner.nextToken());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Get rest of symbols into tokenStream assuming EOF token exists
		while ((tokens.get(tokens.size()-1)).getId()!=Terminals.EOF) {
			try {
				tokens.add(scanner.nextToken());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
/* functions for correction of columns if mixing of tabs and spaces occurs */

	// If a tab is seen, add appropriate amount of columns to the appropriate following tokens
	// and remove the tab token!
	public void columnCorrect() {
		while (pos < tokens.size()) {
			switch (tokens.get(pos).getId()) {	
				case Terminals.TAB:
					addCols();
					removeTab();
					break;
			}
			pos++;
		}
		pos = 0;
	}
	
	// add columns until a newline is started or EOF-tokens is seen
	public void addCols() {
		int index = pos+1;
        while (tokens.get(pos).getLine() == tokens.get(index).getLine() && tokens.get(index).getId() != Terminals.EOF) {
            tokens.get(index).setColumn(tokens.get(index).getColumn()+LangPreprocessor.tabSize-1);
			index++;
        }
    }
	
	public void removeTab() {
		tokens.remove(pos);
		pos--;
	}


/* functions for handling continuation */
	
	// method that should be inserted in case a token can be the reference point for a continuation line
	// mostly probably beginnings of statementokens...
	public void doConTrigger() {
		if(!continuable){	
			continuable = true;
			conColRef = tokens.get(pos).getColumn();
		}
	}
	
	// check whether there is a continuation in progress 
	public void checkCon(short indentType) {
		try {
			checkCon(indentType,0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void checkCon(short indentType, int spaceNo) throws IOException {
		if(	// general conditions
			continuable &&
			tokens.get(pos+1).getLine() > tokens.get(pos).getLine() &&
			
			// conditions for different indentTypes
			((indentType == LangPreprocessor.LESS && tokens.get(pos+1).getColumn() < conColRef) ||
			(indentType == LangPreprocessor.LEQ && tokens.get(pos+1).getColumn() <= conColRef) ||
			(indentType == LangPreprocessor.EQUAL && tokens.get(pos+1).getColumn() == conColRef) ||
			(indentType == LangPreprocessor.MORE && tokens.get(pos+1).getColumn() > conColRef) ||
			(indentType == LangPreprocessor.MEQ && tokens.get(pos+1).getColumn() >= conColRef) ||
			(indentType == LangPreprocessor.EXACT && tokens.get(pos+1).getColumn() == conColRef+spaceNo)))
		{
			int index = pos+1;
			int onLine = tokens.get(pos+1).getLine();
			int deltaLine = tokens.get(pos).getLine()-onLine;
			int deltaCol = tokens.get(pos).getColumn()-onLine;
			// while tokens are on the same line as the token following the continuation trigger
			// pull tokens up to the line of the continuation trigger and offset the column appropriately
			// deltaLine is used for multiple lines of continuation. Should first line be pulled up to 
			// original line, next continuation line needs to be pulled up 2 lines!
				while (tokens.get(index).getLine() == onLine){
					tokens.get(index).setLine(tokens.get(index).getLine()+deltaLine);
					tokens.get(index).setColumn(tokens.get(index).getColumn()+deltaCol);
					index++;
					// to solve end of file problems (array out of bounds) check for invalid index
					// !! could be solved more elegantly with exceptions i'm sure... !!
					if (index >= tokens.size()) {break;}
				}
		// need else if clause in which nothing is done to easier be able to use the else clause...
		} else if(tokens.get(pos+1).getLine() == tokens.get(pos).getLine()) {
		} else {
			addNEWLINE();
			throw new IOException("Invalid end of line at line "+tokens.get(pos).getLine()+". Probable cause: illegal indentation after attempted continuation of line.");
		}
	}
	
	// method "detriggers" continuation line, should be inserted at tokens that start a new statement for example
	public void undoCon() {
		if(pos-1 > 0) {
			if(tokens.get(pos).getLine() > tokens.get(pos-1).getLine()) {
				continuable = false;
			}
		}
	}
	
/* functions for handling blocks */
	
	// method should be inserted whenever a token is seen that is a reference point for a following
	// block element (such as an IF-token)
	public void doBlockTrigger() {
		indentable = true;
		blockColStack.addLast(tokens.get(pos).getColumn());
	}
	
	// checks whether a block is following
	// should be inserted for all tokens that can be followed by a block structure
	public void checkBlock(short indentType) {
		checkBlock(indentType,0);
	}
	
	public void checkBlock(short indentType, int spaceNo) {
		if(	// general conditions
			indentable &&
			tokens.get(pos+1).getLine() > tokens.get(pos).getLine() && 
			tokens.get(pos+1).getId() != Terminals.EOF &&
			blockColStack.size() > 0 &&
			
			// conditions for different indentTypes
			// could be rewritten in switch/case or if structure in order to make it speedier?
			((indentType == LangPreprocessor.LESS && tokens.get(pos+1).getColumn() < blockColStack.getLast()) ||
			(indentType == LangPreprocessor.LEQ && tokens.get(pos+1).getColumn() <= blockColStack.getLast()) ||
			(indentType == LangPreprocessor.EQUAL && tokens.get(pos+1).getColumn() == blockColStack.getLast()) ||
			(indentType == LangPreprocessor.MORE && tokens.get(pos+1).getColumn() > blockColStack.getLast()) ||
			(indentType == LangPreprocessor.MEQ && tokens.get(pos+1).getColumn() >= blockColStack.getLast()) ||
			(indentType == LangPreprocessor.EXACT && tokens.get(pos+1).getColumn() == blockColStack.getLast()+spaceNo))) 
		{
			addINDENT();
		}
	}
	
	// checks whether the token ends the block structure
	public void checkUnBlock(short indentType) {
		checkUnBlock(indentType,0);
	}
	
	public void checkUnBlock(short indentType, int spaceNo) {
		while (	
			// general conditions
			pos-1 >= 0 &&
			tokens.get(pos).getLine() > tokens.get(pos-1).getLine() && 
			tokens.get(pos).getId() != Terminals.EOF &&
			blockColStack.size() > 0 &&
			
			// check for interdependencies
			// seemingly wrong and/or unnecessary code, test thoroughly then redevise
			// (!checkBlockInteraction(indentType, spaceNo)) &&
			
			// conditions for different indentTypes
			((indentType == LangPreprocessor.LESS && tokens.get(pos).getColumn() >= blockColStack.getLast()) ||
			(indentType == LangPreprocessor.LEQ && tokens.get(pos).getColumn() > blockColStack.getLast()) ||
			(indentType == LangPreprocessor.EQUAL && tokens.get(pos).getColumn() != blockColStack.getLast()) ||
			(indentType == LangPreprocessor.MORE && tokens.get(pos).getColumn() <= blockColStack.getLast()) ||
			(indentType == LangPreprocessor.MEQ && tokens.get(pos).getColumn() < blockColStack.getLast()) ||
			(indentType == LangPreprocessor.EXACT && tokens.get(pos).getColumn() != blockColStack.getLast()+spaceNo))) 
		{
			addDEDENT();
		}
	}
	
	// seemingly wrong and/or unnecessary code
	public boolean checkBlockInteraction(short indentType, int spaceNo) {
		if(	// conditions for different indentTypes
			(indentType == LangPreprocessor.LESS && tokens.get(pos).getColumn() < tokens.get(pos-1).getColumn()) ||
			(indentType == LangPreprocessor.LEQ && tokens.get(pos).getColumn() <= tokens.get(pos-1).getColumn()) ||
			(indentType == LangPreprocessor.EQUAL && tokens.get(pos).getColumn() == tokens.get(pos-1).getColumn()) ||
			(indentType == LangPreprocessor.MORE && tokens.get(pos).getColumn() > tokens.get(pos-1).getColumn()) ||
			(indentType == LangPreprocessor.MEQ && tokens.get(pos).getColumn() >= tokens.get(pos-1).getColumn()) ||
			(indentType == LangPreprocessor.EXACT && tokens.get(pos).getColumn() == tokens.get(pos-1).getColumn()+spaceNo)) {
			return true;
		}
		return false;
	}
	
/* functions handling newlines - mostly necessary when NEWLINE token is not given by the scanner but needs to be inserted */
	
	// Check whether a NEWLINE token should be inserted, used at tokens that are allowed to start a new line
	public void checkNewline() {
		if(	pos-1 >= 0  && 
			tokens.get(pos).getLine() > tokens.get(pos-1).getLine() &&
			tokens.get(pos-1).getId() != Terminals.INDENT			){
			addNEWLINE();
		}
	}
	
	// Actually quite similar to checkNewLine just that standard version checks whether the next token is on 
	// a line with a bigger line count than current token.
	// Also this function adds a NEWLINE token in order to produce a parsing error.
	// Should be used on tokens that are not allowed to be followed by a NEWLINE!
	public void checkNoNewline() {
		checkNoNewline(1, 0);
	}
	
	public void checkNoNewline(int a, int b) {
		a+=pos;
		b+=pos;
		if(tokens.get(a).getLine() > tokens.get(b).getLine()) 
		{
			addNEWLINE(a);
			if(a <= pos) {
				pos++;
			}
		}
	}
	
/* functions for insertion of tokens */
	
	// SUGGESTION: MAKE MORE GENERAL BY INSERTING ARGUMENT -> addINDENT(int position) with overloaded default
	public void addINDENT() {
		if(LangPreprocessor.manualcheck){System.out.println("Responsible for INDENT: |"+Terminals.NAMES[tokens.get(pos).getId()]+"| - l:"+tokens.get(pos).getLine()+" c:"+tokens.get(pos).getColumn());}
		tokens.add(pos+1, new LPSymbol(Terminals.INDENT, tokens.get(pos).getLine(), tokens.get(pos).getColumn(), 0));
		indentable = false;
	}
	
	// SUGGESTION: MAKE MORE GENERAL BY INSERTING ARGUMENT -> addDEDENT(int position) with overloaded default
	public void addDEDENT() {
		if(LangPreprocessor.manualcheck){
			System.out.println("Responsible for DEDENT: |"+Terminals.NAMES[tokens.get(pos).getId()]+"| - l:"+tokens.get(pos).getLine()+" c:"+tokens.get(pos).getColumn());
			System.out.println("THIS TOKEN COL: "+tokens.get(pos).getColumn());
			System.out.println("STACK COL: "+blockColStack.getLast());
		}
		tokens.add(pos, new LPSymbol(Terminals.DEDENT, tokens.get(pos-1).getLine(), tokens.get(pos-1).getColumn(), 0));
		blockColStack.removeLast();
		pos++;
	}
	
	// NOT ENTIRELY SURE WHAT THIS IS FOR AND IF STILL NEEDED!
	public void addDEDENT(short special) {
		while (blockColStack.size() > 0) {
			tokens.add(pos, new LPSymbol(Terminals.DEDENT, tokens.get(pos-1).getLine(), tokens.get(pos-1).getColumn(), 0));
			blockColStack.removeLast();
			pos++;
		}
	}
	
	public void addNEWLINE() {
		addNEWLINE(pos);
	}
	
	public void addNEWLINE(int position) {
		if(LangPreprocessor.manualcheck){System.out.println("Responsible for NEWLINE: |"+Terminals.NAMES[tokens.get(pos).getId()]+"| - l:"+tokens.get(pos).getLine()+" c:"+tokens.get(pos).getColumn());}
		tokens.add(position, new LPSymbol(Terminals.NEWLINE, tokens.get(position-1).getLine(), tokens.get(position-1).getColumn(), 0));
		
		if(position <= pos) {
			pos++;
		}
	}
}
package lang;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.ArrayDeque;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;
import lang.ast.LangScanner;

import beaver.*;

public class LangPreprocessor extends beaver.Scanner {
	
	static public boolean manualcheck = false; // enable standardprintouts for manual checking...
	
	private TokenStream ts; // token stream
	
    static public int tabSize = 4; // in order to get correct column-counts
	
	private int globalCounter = 0; // global counter for nextToken-method
	
	// block indentation options
	static public final short LESS = 0;
	static public final short LEQ = 1;
	static public final short EQUAL = 2;
	static public final short MORE = 3;
	static public final short MEQ = 4;
	static public final short EXACT = 5;
    
    // Constructor - give it the entire token stream
	public LangPreprocessor (LangScanner scanner) {
		
		// create token stream object
		ts = new TokenStream (scanner);
		
        // Print unedited tokenstream
        if(manualcheck) {
            for(LPSymbol s: ts.tokens) {
                System.out.println("|"+Terminals.NAMES[s.getId()]+"| - l:"+s.getLine()+" c:"+s.getColumn());
            }
			
            System.out.println("_________");
			System.out.println("");
		}
		
		// Correct column-numbers and delete tabs
		ts.columnCorrect();
		
		// Call function that inserts needed INDENT and DEDENT tokens
		editTokenStream();
	}
	
/* functions that manage editing of the token stream */
    
	public void editTokenStream() {
		tsloop:
		while (ts.pos < ts.tokens.size()) {
			if(manualcheck){System.out.println("i = "+ts.pos+" with Token |"+Terminals.NAMES[ts.tokens.get(ts.pos).getId()]+"|");}
			
			switch (ts.tokens.get(ts.pos).getId()) {
				case Terminals.EOF:
					ts.addDEDENT(Terminals.EOF);
					break tsloop;
				case Terminals.ID:
					ts.checkUnBlock(EXACT,2);
					ts.undoCon();
					ts.doConTrigger();
					ts.checkBlock(EXACT,2);
					ts.checkNewline();
					break;
				case Terminals.LPAR:
					ts.checkNoNewline();
					break;
				case Terminals.NUMERAL:
					ts.checkBlock(EXACT,2);
					break;
				case Terminals.NEWLINE: break;
				case Terminals.SEQ:
					ts.checkUnBlock(EXACT,2);
					ts.undoCon();
					ts.doBlockTrigger();
					ts.doConTrigger();
					ts.checkBlock(EXACT,2);
					ts.checkNewline();
					break;
				case Terminals.COMMA:
					ts.checkNoNewline(0,-1);
					ts.checkCon(MORE);
					break;
				case Terminals.EQ:
					ts.checkNoNewline();
					break;
				case Terminals.FOR: 
					ts.checkCon(MORE);
					ts.checkNoNewline(0,-1);
					break;
				case Terminals.INDENT: break;
				case Terminals.DEDENT: break;
				case Terminals.RPAR:
					ts.checkBlock(EXACT,2);
					break;
				case Terminals.ASSIGN: 
					ts.checkCon(MORE);
					ts.checkNoNewline(0,-1);
					break;
				case Terminals.PLUS: 
					ts.checkCon(MORE);
					ts.checkNoNewline(0,-1);
					break;
				case Terminals.MINUS: 
					ts.checkCon(MORE);
					ts.checkNoNewline(0,-1);
					break;
				default: break;
			}
			ts.pos++;
		}
		
		// Print out tokens
        if(manualcheck) {
			System.out.println("_________");
			System.out.println("");
            for(int j=0; j < ts.tokens.size(); j++) {
                System.out.println("|"+Terminals.NAMES[ts.tokens.get(j).getId()]+"| - l:"+ts.tokens.get(j).getLine()+" c:"+ts.tokens.get(j).getColumn());
            }
        }
	}

/* connector to beaver parser */
	
	public beaver.Symbol nextToken() throws java.io.IOException {
		// simple function that beaver uses to get the next token
		globalCounter++;
		return new beaver.Symbol (  ts.tokens.get(globalCounter-1).getId(), 
                                    ts.tokens.get(globalCounter-1).getLine(),
                                    ts.tokens.get(globalCounter-1).getColumn(),
                                    ts.tokens.get(globalCounter-1).getLength(),
                                    ts.tokens.get(globalCounter-1).getValue()    );
	}	
}
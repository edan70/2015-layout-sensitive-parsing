package lang;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.ArrayDeque;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;
import lang.ast.LangScanner;
import lang.LPSymbol;

import beaver.*;

public class LangPreprocessor extends beaver.Scanner {
	
	boolean manualcheck = false; // enable standardprintouts for manual checking...
	
	private ArrayList<LPSymbol> ts = new ArrayList<LPSymbol>(); // tokenStream
    private int tabSize = 4; // in order to get correct column-counts
	private int globalCounter = 0; // global counter for nextToken-method
	
	private int pos = 0; // global counter for all methods that play with the tokenSteam
	private boolean indentable = false;
	private boolean continuable = false;
	private Deque<Integer> blockColStack = new ArrayDeque<Integer>();
	private int conColRef;
	
	// block indentation options
	static public final short LESS 	= 0;
	static public final short LEQ 	= 1;
	static public final short EQUAL = 2;
	static public final short MORE 	= 3;
	static public final short MEQ 	= 4;
	static public final short EXACT = 5;
    
    // Constructor - give it the entire token stream
	public LangPreprocessor (LangScanner scanner) {
		// Get first element into the ArrayList
		try {
			ts.add(scanner.nextToken());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Get rest of symbols into tokenStream assuming EOF token exists
		while ((ts.get(ts.size()-1)).getId()!=Terminals.EOF) {
			try {
				ts.add(scanner.nextToken());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
        // Print unedited tokenstream
        if(manualcheck) {
            for(LPSymbol s: ts) {
                System.out.println("|"+Terminals.NAMES[s.getId()]+"| - l:"+s.getLine()+" c:"+s.getColumn());
            }
			
            System.out.println("_________");
			System.out.println("");
		}
		
		// Correct column-numbers and delete tabs
		columnCorrect();
		
		// Call function that inserts needed INDENT and DEDENT tokens
		editTokenStream();
	}
	
/* functions that manage editing of the token stream */
	
	public void columnCorrect() {
		while (pos < ts.size()) {
			switch (ts.get(pos).getId()) {	
				case Terminals.TAB:
					addCols();
					removeTab();
					break;
			}
			pos++;
		}
		pos = 0;
	}
    
	public void editTokenStream() {
		tsloop:
		while (pos < ts.size()) {
			if(manualcheck){System.out.println("i = "+pos+" with Token |"+Terminals.NAMES[ts.get(pos).getId()]+"|");}
			
			switch (ts.get(pos).getId()) {
				case Terminals.EOF:
					addDEDENT(Terminals.EOF);
					break tsloop;
				case Terminals.ID:
					checkUnBlock(EXACT,2);
					undoCon();
					doConTrigger();
					checkBlock(EXACT,2);
					checkNewline();
					break;
				case Terminals.LPAR:
					checkNoNewline();
					break;
				case Terminals.NUMERAL:
					checkBlock(EXACT,2);
					break;
				case Terminals.NEWLINE: break;
				case Terminals.SEQ:
					checkUnBlock(EXACT,2);
					undoCon();
					doBlockTrigger();
					doConTrigger();
					checkBlock(EXACT,2);
					checkNewline();
					break;
				case Terminals.COMMA:
					checkNoNewline(pos,pos-1);
					checkCon(MORE);
					break;
				case Terminals.EQ:
					checkNoNewline();
					break;
				case Terminals.FOR: 
					checkCon(MORE);
					checkNoNewline(pos,pos-1);
					break;
				case Terminals.INDENT: break;
				case Terminals.DEDENT: break;
				case Terminals.RPAR:
					checkBlock(EXACT,2);
					break;
				case Terminals.ASSIGN: 
					checkCon(MORE);
					checkNoNewline(pos,pos-1);
					break;
				case Terminals.PLUS: 
					checkCon(MORE);
					checkNoNewline(pos,pos-1);
					break;
				case Terminals.MINUS: 
					checkCon(MORE);
					checkNoNewline(pos,pos-1);
					break;
				default: break;
			}
			pos++;
		}
		
		// Print out tokens
        if(manualcheck) {
			System.out.println("_________");
			System.out.println("");
            for(int j=0; j < ts.size(); j++) {
                System.out.println("|"+Terminals.NAMES[ts.get(j).getId()]+"| - l:"+ts.get(j).getLine()+" c:"+ts.get(j).getColumn());
            }
        }
	}
	
/* HELPER FUNCTIONS */
/* functions for handling tab-space issues */
	
	public void addCols() {
		int index = pos+1;
        while (ts.get(pos).getLine() == ts.get(index).getLine() && ts.get(index).getId() != Terminals.EOF) {
            ts.get(index).setColumn(ts.get(index).getColumn()+tabSize-1);
			index++;
        }
    }
	
	public void removeTab() {
		ts.remove(pos);
		pos--;
	}
	
/* functions for handling continuation */
	
	public void doConTrigger() {
		if(!continuable){	
			continuable = true;
			conColRef = ts.get(pos).getColumn();
		}
	}
	
	public void checkCon(short indentType) {
		try {
			checkCon(indentType,0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void checkCon(short indentType, int spaceNo) throws IOException {
		if(	// general conditions
			continuable &&
			ts.get(pos+1).getLine() > ts.get(pos).getLine() &&
			
			// conditions for different indentTypes
			((indentType == LESS && ts.get(pos+1).getColumn() < conColRef) ||
			(indentType == LEQ && ts.get(pos+1).getColumn() <= conColRef) ||
			(indentType == EQUAL && ts.get(pos+1).getColumn() == conColRef) ||
			(indentType == MORE && ts.get(pos+1).getColumn() > conColRef) ||
			(indentType == MEQ && ts.get(pos+1).getColumn() >= conColRef) ||
			(indentType == EXACT && ts.get(pos+1).getColumn() == conColRef+spaceNo)))
		{
			int index = pos+1;
			int onLine = ts.get(pos+1).getLine();
			int deltaLine = ts.get(pos).getLine()-onLine;
			int deltaCol = ts.get(pos).getColumn()-onLine;
				while (ts.get(index).getLine() == onLine){
					ts.get(index).setLine(ts.get(index).getLine()+deltaLine);
					ts.get(index).setColumn(ts.get(index).getColumn()+deltaCol);
					index++;
					if (index >= ts.size()) {break;}
				}
		} else if(ts.get(pos+1).getLine() == ts.get(pos).getLine()) {
		} else {
			addNEWLINE();
			throw new IOException("Invalid end of line at line "+ts.get(pos).getLine()+". Probable cause: illegal indentation after attempted continuation of line.");
		}
	}
	
	public void undoCon() {
		if(pos-1 > 0) {
			if(ts.get(pos).getLine() > ts.get(pos-1).getLine()) {
				continuable = false;
			}
		}
	}
	
/* functions for handling blocks */
	
	public void doBlockTrigger() {
		indentable = true;
		blockColStack.addLast(ts.get(pos).getColumn());
	}
	
	public void checkBlock(short indentType) {
		checkBlock(indentType,0);
	}
	
	public void checkBlock(short indentType, int spaceNo) {
		if(	// general conditions
			indentable &&
			ts.get(pos+1).getLine() > ts.get(pos).getLine() && 
			ts.get(pos+1).getId() != Terminals.EOF &&
			blockColStack.size() > 0 &&
			
			// conditions for different indentTypes
			((indentType == LESS && ts.get(pos+1).getColumn() < blockColStack.getLast()) ||
			(indentType == LEQ && ts.get(pos+1).getColumn() <= blockColStack.getLast()) ||
			(indentType == EQUAL && ts.get(pos+1).getColumn() == blockColStack.getLast()) ||
			(indentType == MORE && ts.get(pos+1).getColumn() > blockColStack.getLast()) ||
			(indentType == MEQ && ts.get(pos+1).getColumn() >= blockColStack.getLast()) ||
			(indentType == EXACT && ts.get(pos+1).getColumn() == blockColStack.getLast()+spaceNo))) 
		{
			addINDENT();
		}
	}
	
	public void checkUnBlock(short indentType) {
		checkUnBlock(indentType,0);
	}
	
	public void checkUnBlock(short indentType, int spaceNo) {
		while (	
			// general conditions
			pos-1 >= 0 &&
			ts.get(pos).getLine() > ts.get(pos-1).getLine() && 
			ts.get(pos).getId() != Terminals.EOF &&
			blockColStack.size() > 0 &&
			
			// check for interdependencies
			// (!checkBlockInteraction(indentType, spaceNo)) &&
			
			// conditions for different indentTypes
			((indentType == LESS && ts.get(pos).getColumn() >= blockColStack.getLast()) ||
			(indentType == LEQ && ts.get(pos).getColumn() > blockColStack.getLast()) ||
			(indentType == EQUAL && ts.get(pos).getColumn() != blockColStack.getLast()) ||
			(indentType == MORE && ts.get(pos).getColumn() <= blockColStack.getLast()) ||
			(indentType == MEQ && ts.get(pos).getColumn() < blockColStack.getLast()) ||
			(indentType == EXACT && ts.get(pos).getColumn() != blockColStack.getLast()+spaceNo))) 
		{
			addDEDENT();
		}
	}
	
	// seemingly wrong and/or unnecessary code
	public boolean checkBlockInteraction(short indentType, int spaceNo) {
		if(	// conditions for different indentTypes
			(indentType == LESS && ts.get(pos).getColumn() < ts.get(pos-1).getColumn()) ||
			(indentType == LEQ && ts.get(pos).getColumn() <= ts.get(pos-1).getColumn()) ||
			(indentType == EQUAL && ts.get(pos).getColumn() == ts.get(pos-1).getColumn()) ||
			(indentType == MORE && ts.get(pos).getColumn() > ts.get(pos-1).getColumn()) ||
			(indentType == MEQ && ts.get(pos).getColumn() >= ts.get(pos-1).getColumn()) ||
			(indentType == EXACT && ts.get(pos).getColumn() == ts.get(pos-1).getColumn()+spaceNo)) {
			return true;
		}
		return false;
	}
	
/* functions handling newlines */
	
	public void checkNewline() {
		if(	pos-1 >= 0  && 
			ts.get(pos).getLine() > ts.get(pos-1).getLine() &&
			ts.get(pos-1).getId() != Terminals.INDENT			){
			addNEWLINE();
		}
	}
	
	public void checkNoNewline() {
		checkNoNewline(pos+1, pos);
	}
	
	public void checkNoNewline(int a, int b) {
		if(ts.get(a).getLine() > ts.get(b).getLine()) 
		{
			addNEWLINE(a);
			if(a <= pos) {
				pos++;
			}
		}
	}
	
/* functions for insertion of tokens */
	
	public void addINDENT() {
		if(manualcheck){System.out.println("Responsible for INDENT: |"+Terminals.NAMES[ts.get(pos).getId()]+"| - l:"+ts.get(pos).getLine()+" c:"+ts.get(pos).getColumn());}
		ts.add(pos+1, new LPSymbol(Terminals.INDENT, ts.get(pos).getLine(), ts.get(pos).getColumn(), 0));
		indentable = false;
	}
	
	public void addDEDENT() {
		if(manualcheck){
			System.out.println("Responsible for DEDENT: |"+Terminals.NAMES[ts.get(pos).getId()]+"| - l:"+ts.get(pos).getLine()+" c:"+ts.get(pos).getColumn());
			System.out.println("THIS TOKEN COL: "+ts.get(pos).getColumn());
			System.out.println("STACK COL: "+blockColStack.getLast());
		}
		ts.add(pos, new LPSymbol(Terminals.DEDENT, ts.get(pos-1).getLine(), ts.get(pos-1).getColumn(), 0));
		blockColStack.removeLast();
		pos++;
	}
	
	public void addDEDENT(short special) {
		while (blockColStack.size() > 0) {
			ts.add(pos, new LPSymbol(Terminals.DEDENT, ts.get(pos-1).getLine(), ts.get(pos-1).getColumn(), 0));
			blockColStack.removeLast();
			pos++;
		}
	}
	
	public void addNEWLINE() {
		addNEWLINE(pos);
	}
	
	public void addNEWLINE(int position) {
		if(manualcheck){System.out.println("Responsible for NEWLINE: |"+Terminals.NAMES[ts.get(pos).getId()]+"| - l:"+ts.get(pos).getLine()+" c:"+ts.get(pos).getColumn());}
		ts.add(position, new LPSymbol(Terminals.NEWLINE, ts.get(position-1).getLine(), ts.get(position-1).getColumn(), 0));
		
		if(position <= pos) {
			pos++;
		}
	}

/* connector to beaver parser */
	
	public beaver.Symbol nextToken() throws java.io.IOException {
		// simple function that beaver uses to get the next token
		globalCounter++;
		return new beaver.Symbol (  ts.get(globalCounter-1).getId(), 
                                    ts.get(globalCounter-1).getLine(),
                                    ts.get(globalCounter-1).getColumn(),
                                    ts.get(globalCounter-1).getLength(),
                                    ts.get(globalCounter-1).getValue()    );
	}	
}
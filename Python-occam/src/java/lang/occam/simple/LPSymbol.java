package lang;

import lang.ast.LangParser.Terminals;

public class LPSymbol extends beaver.Symbol{
    
    private int line;
    private int column;
    private int eLine;
    private int eColumn;
	private String name;
    
    private int length;
    private Object value;
    
    public LPSymbol(short id, int start_line, int start_column, int length) {
		super(id, start_line, start_column, length);
		line = start_line;
		column = start_column;
		this.length = length;
		this.name = Terminals.NAMES[id];
	}
	
	public LPSymbol(short id, int start_line, int start_column, int length, Object value) {
        super(id, start_line, start_column, value);
        line = start_line;
        column = start_column;
        this.length = length;
        this.value = value;
		this.name = Terminals.NAMES[id];
    }
    
	// Getters and setters
    public int getLine() { return this.line; }
    public void setLine(int line) { this.line = line; }
    
    public int getColumn() { return this.column; }
    public void setColumn(int column) { this.column = column; }
    
    public int getELine() { return this.eLine; }
    public void setELine(int eLine) { this.eLine = eLine; }
    
    public int getEColumn() { return this.eColumn; }
    public void setEColumn(int eColumn) { this.eColumn = eColumn; }
	
	public String getName() { return this.name; }

	public int getLength() { return this.length; }
	
	public Object getValue() { return this.value; }
	public void setValue(Object value) { this.value = value; }
}
package lang;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.ArrayDeque;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;
import lang.ast.LangScanner;

import beaver.*;

public class LangPreprocessor extends beaver.Scanner {
	
	// ts - tokenStream
	ArrayList<beaver.Symbol> ts = new ArrayList<beaver.Symbol>();
	
	// structure in order to get correct column-counts
    ArrayList<Integer> addedCols = new ArrayList<Integer>();
    int tabSize = 4;
	
	// Counter for nextToken-method
	int counter = 0;
    
    // enable standardprintouts for manual checking...
    boolean manualcheck = false;
	
	// Instantiate LangPreprocessor by giving it the entire token stream
	public LangPreprocessor (LangScanner scanner) {
		// Get first element into the ArrayList
		try {
			ts.add(scanner.nextToken());
            addedCols.add(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Get rest of symbols into tokenStream
		// NEED GUARD AGAINST NO EOF!!!
		while ((ts.get(ts.size()-1)).getId()!=Terminals.EOF) {
			try {
				ts.add(scanner.nextToken());
                addedCols.add(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
        // Print unedited tokenstream
        if(manualcheck) {
            for(beaver.Symbol s: ts) {
                System.out.println("|"+Terminals.NAMES[s.getId()]+"| - l:"+s.getLine(s.getStart())+" c:"+s.getColumn(s.getStart()));
            }
			
            System.out.println("_________");
			System.out.println("");
		}
        
		// Call function that inserts needed INDENT and DEDENT tokens
		editTokenStream(scanner);
	}
	
	public void editTokenStream(LangScanner scanner){
		int i = 0;
		boolean indentable = false;
		boolean newlined = false;
		Deque<Integer> indentStack = new ArrayDeque<Integer>();
		
		tsloop:
		while (i < ts.size()) {
            if(manualcheck){System.out.println("i = "+i+" with Token |"+Terminals.NAMES[ts.get(i).getId()]+"|");}
            
            /*  
            
            Introduce case for every possible token:
            -> every token that starts statement with offside-block 
                    - need for setting indentable and pushing its column onto indentStack
                    here:   IF, ELIF, ELSE
            -> EOF - need for DEDENT until indentStack is empty and deletion of any trailing NEWLINES.
					It might be safer not to use newlined here as a criterium but rather ask
					whether the n last tokens were NEWLINES and then delete those.
					Also break out of entire loop when EOF is seen.					
            -> every token that is FIRST(stmt) - need for DEDENT in case indentation is lower than offside-block
					also, newlined needs to be set to false
                    here:   FIRST(assign_stmt) = ID 
                            FIRST(if_stmt) = IF
                            FIRST(else_stmt) = ELSE
                            FIRST(elif_stmt) = ELIF
            -> NEWLINE - if seen when indentable is true, need for INDENT-insertion
					if seen when newlined is true, need for deletion of the NEWLINE-token
					also sets newlined to true (problems arises if empty newlines want to valid syntax-wise)
            -> every token that CAN only be seen after a DEDENT
                    - set the first DEDENT manually if you want to allow that token to be
                    indented further than the top of the indentStack.
                    here:   ELIF, ELSE
            -> above mentioned stmts that can only occur after DEDENT and that need the NEWLINE
                    before eliminated in order to resolve ambiguity-issues
                    - remove said NEWLINE-token
                    here:   ELIF, ELSE
			-> TAB - remove TAB-token and add tabSize to all tokens' addedCols
					following the tab until NEWLINE appears
            -> all other tokens - don't take any action: break; 
                    those cases could just be left out and handled by default
                    here: COLON, NUMERAL, INDENT, DEDENT, ASSING, EQ
                    
            */
            
            switch (ts.get(i).getId()){
                case Terminals.EOF:
                    
					if(newlined) {
						ts.remove(i-1);
						addedCols.remove(i-1);
						i--;
					}
					
					while (indentStack.size() > 0) {
                        ts.add(i, scanner.sym(Terminals.DEDENT));
                        addedCols.add(i,0);
                        indentStack.removeLast();
                    }
					
                    break tsloop;
                case Terminals.ID:    
                    while (indentStack.size() > 0 && indentStack.getLast() >= col(i)) {
                        ts.add(i-1, scanner.sym(Terminals.DEDENT));
                        addedCols.add(i-1,0);
                        indentStack.removeLast();
                        i++;
                    }
					
					newlined = false;
                    break;
                case Terminals.NEWLINE:
                    if(indentable) {
                        ts.add(i+1, scanner.sym(Terminals.INDENT));
                        addedCols.add(i+1,0);
                        indentable = false;
                    }
					if(newlined) {
						ts.remove(i);
						addedCols.remove(i);
						i--;
					}
					newlined = true;
                    break;
                case Terminals.COLON: break;
                case Terminals.NUMERAL: break;
                case Terminals.IF:
                    while (indentStack.size() > 0 && indentStack.getLast() >= col(i)) {
                        ts.add(i-1, scanner.sym(Terminals.DEDENT));
                        addedCols.add(i-1,0);
                        indentStack.removeLast();
                        i++;
                    }
                    
                    indentable = true;
                    indentStack.addLast(col(i));
					newlined = false;
                    break;
                case Terminals.ELIF:
                    ts.remove(i-1);
                    addedCols.remove(i-1);
                    i--;
                    
                    ts.add(i, scanner.sym(Terminals.DEDENT));
                    addedCols.add(i,0);
                    if(indentStack.size() > 0) {indentStack.removeLast();}
                    i++;
                    
                    while (indentStack.size() > 0 && col(i) <= indentStack.getLast()) {
                        ts.add(i, scanner.sym(Terminals.DEDENT));
                        addedCols.add(i,0);
                        indentStack.removeLast();
                        i++;
                    }
                    
                    indentable = true;
                    indentStack.addLast(col(i));
					newlined = false;
                    break;
                case Terminals.INDENT:  break;
                case Terminals.DEDENT:  break;
                case Terminals.ELSE:
                    ts.remove(i-1);
                    addedCols.remove(i-1);
                    i--;
                    
                    ts.add(i, scanner.sym(Terminals.DEDENT));
                    addedCols.add(i,0);
                    if(indentStack.size() > 0) {indentStack.removeLast();}
                    i++;
                
                    while (indentStack.size() > 0 && indentStack.getLast() >= col(i)) {
                        ts.add(i-1, scanner.sym(Terminals.DEDENT));
                        addedCols.add(i-1,0);
                        indentStack.removeLast();
                        i++;
                    }
                    
                    indentable = true;
                    indentStack.addLast(col(i));
					newlined = false;
                    break;
                case Terminals.ASSIGN:  break;
                case Terminals.TAB:
                    addCols(i);
                    ts.remove(i);
                    addedCols.remove(i);
                    i--;
					break;
                case Terminals.EQ:      break;
                default:                break;
            }
			i++;
		}
		
        // Print out tokens
        if(manualcheck) {
			System.out.println("_________");
			System.out.println("");
            for(int j=0; j < ts.size(); j++) {
                System.out.println("|"+Terminals.NAMES[ts.get(j).getId()]+"| - l:"+ts.get(j).getLine(ts.get(j).getStart())+" c:"+col(j));
            }
        }
	}
    
    public void addCols(int index) {
        while (ts.get(index).getId() != Terminals.NEWLINE && ts.get(index).getId() != Terminals.EOF) {
            addedCols.set(index, addedCols.get(index) + tabSize-1);
			index++;
        }
    }
    
	// helper for easier access to column of ts
	public int col(int index) {
		int bla = ts.get(index).getColumn(ts.get(index).getStart()) + addedCols.get(index);
		return ts.get(index).getColumn(ts.get(index).getStart()) + addedCols.get(index);
	}
    
	public beaver.Symbol nextToken() throws java.io.IOException {
		// simple function that beaver uses to get the next token
		counter++;
		return ts.get(counter-1);
	}	
}
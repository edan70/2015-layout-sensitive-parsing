package lang;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.ArrayDeque;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;
import lang.ast.LangScanner;

import beaver.*;

public class LangPreprocessor extends beaver.Scanner {
	
	// ts - tokenStream
	ArrayList<beaver.Symbol> ts = new ArrayList<beaver.Symbol>();
	int counter = 0;
    
    // enable standardprintouts for manual checking...
    boolean manualcheck = false;
	
	// Instantiate LangPreprocessor by giving it the entire token stream
	public LangPreprocessor (LangScanner scanner) {
		// Get first element into the ArrayList
		try {
			ts.add(scanner.nextToken());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Get rest of symbols into tokenStream
		// NEED GUARD AGAINST NO EOF!!!
		while ((ts.get(ts.size()-1)).getId()!=Terminals.EOF) {
			try {
				ts.add(scanner.nextToken());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
        // Print unedited tokenstream
        if(manualcheck) {
            for(beaver.Symbol s: ts) {
                System.out.println("|"+Terminals.NAMES[s.getId()]+"| - l:"+s.getLine(s.getStart())+" c:"+s.getColumn(s.getStart()));
            }
		
            System.out.println("_________");
		}
        
		// Call function that inserts needed INDENT and DEDENT tokens
		editTokenStream(scanner);
	}
	
	public void editTokenStream(LangScanner scanner){
		int i = 0;
		boolean indentable = false;
		Deque<Integer> indentStack = new ArrayDeque<Integer>();
		
		while (i < ts.size()) {
                //System.out.println("i = "+i+" with Token |"+Terminals.NAMES[ts.get(i).getId()]+"|");
			// if IF is seen, set indentable to true and put the column of
			// IF on indentStack
			if (ts.get(i).getId() == Terminals.IF) {
                // Since the if_stmt is a regular stmt (just as an assignment...)
                // we have to check which subtree it belongs to 
                // (analogous to what happens later on for all other tokens!)
                while (indentStack.size() > 0 && indentStack.getLast() >= col(ts,i)) {
                        //System.out.println("Stackvalue: "+indentStack.getLast());
                        //System.out.println("Colvalue: "+col(ts,i)+" at "+NAMES[ts.get(i).getId()]);
					// Add DEDENT before NEWLINE that was before current token (so at i-1)
					ts.add(i-1, scanner.sym(Terminals.DEDENT));
					indentStack.removeLast();
                    i++;
				}
                
				indentable = true;
				indentStack.addLast(col(ts,i));
                
            // if ELIF or ELSE is seen, replace the NEWLINE (i-1) by a SNEWLINE (special newline)
            // insert a certain amount of DEDENTs, set indentable to true and add column of ELSE to indentStack
            // also, increment i or infinite loop occurs
            } else if (ts.get(i).getId() == Terminals.ELIF || ts.get(i).getId() == Terminals.ELSE) {
                //ts.set(i-1, scanner.sym(Terminals.SNEWLINE)); <--
                ts.remove(i-1);
                i--;
                
                // First DEDENT has to happen regardless of WHERE the ELSE is
                // This caters to it being able to also be more indented than the IF
                ts.add(i, scanner.sym(Terminals.DEDENT));
                if(indentStack.size() > 0) {indentStack.removeLast();}
                i++;
                
                // First condition value safeguard against if-else on line 1
                while (indentStack.size() > 0 && col(ts,i) <= indentStack.getLast()) {
                    ts.add(i, scanner.sym(Terminals.DEDENT));
                    indentStack.removeLast();
                    i++;
                }
                
                indentable = true;
				indentStack.addLast(col(ts,i));
			
			// If a NEWLINE is seen after IF (thus, indentable is true) 
			// insert INDENT token and set indentable to false
			} else if (ts.get(i).getId() == Terminals.NEWLINE && indentable) {
				// Add INDENT after current token (so at i+1)
				ts.add(i+1, scanner.sym(Terminals.INDENT));
				indentable = false;
            
			// don't do anything in case of NEWLINE while indentable is false
            // and some other tokens
			} else if ( ts.get(i).getId() == Terminals.NEWLINE  || 
                        ts.get(i).getId() == Terminals.COLON    || 
                        ts.get(i).getId() == Terminals.INDENT   ||      
                        ts.get(i).getId() == Terminals.DEDENT       ) {
			
			// should EOF be reached and the stack of indents is not empty,
			// add as many DEDENTS as there are stack-elements
			} else if (ts.get(i).getId() == Terminals.EOF) {
				while (indentStack.size() > 0) {
					ts.add(i, scanner.sym(Terminals.DEDENT));
					indentStack.removeLast();
				}
			
			// In all other cases check whether following tokens are more indented 
			// than current indentStack, if they are less indented, insert DEDENT-token
			// pop last element off the stack and compare again...
            // SHOULD NOT BE RELEVANT FOR INDENT, DEDENT, COLON but only for beginnings of stmts
            // Can be especially be problematic with inserted tokens, since their col-value is not correct!
            // CHECK ON NECESSITY OF THIRD CONDITION... SHOULD BE REDUNDANT
			} else {
				while (indentStack.size() > 0 && indentStack.getLast() >= col(ts,i) && ts.get(i).getId() != Terminals.NEWLINE) {
					// Add DEDENT before NEWLINE that was before current token (so at i-1)
					ts.add(i-1, scanner.sym(Terminals.DEDENT));
					indentStack.removeLast();
                    i++;
				}
			}
			i++;
		}
		
        // Print out tokens
        if(manualcheck) {    
            for(beaver.Symbol s: ts) {
                System.out.println("|"+Terminals.NAMES[s.getId()]+"| - l:"+s.getLine(s.getStart())+" c:"+s.getColumn(s.getStart()));
            }
        }
	}

	// helper for easier access to column of tokenStream
	public int col(ArrayList<beaver.Symbol> tokenStream, int index) {
		return tokenStream.get(index).getColumn(tokenStream.get(index).getStart());
	}
	
	public beaver.Symbol nextToken() throws java.io.IOException {
		// simple function that beaver uses to get the next token
		counter++;
		return ts.get(counter-1);
	}	
}
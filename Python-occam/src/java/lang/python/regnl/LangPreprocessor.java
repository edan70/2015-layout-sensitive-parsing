package lang;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.ArrayDeque;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;
import lang.ast.LangScanner;

import beaver.*;

public class LangPreprocessor extends beaver.Scanner {
	
	// ts - tokenStream
	ArrayList<beaver.Symbol> ts = new ArrayList<beaver.Symbol>();
	int counter = 0;
	
	// Instantiate LangPreprocessor by giving it the entire token stream
	public LangPreprocessor (LangScanner scanner) {
		// Get first element into the ArrayList
		try {
			ts.add(scanner.nextToken());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Get rest of symbols into tokenStream
		// NEED GUARD AGAINST NO EOF!!!
		while ((ts.get(ts.size()-1)).getId()!=0) {
			try {
				ts.add(scanner.nextToken());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		for(beaver.Symbol s: ts) {
			System.out.println("|"+Terminals.NAMES[s.getId()]+"| - l:"+s.getLine(s.getStart())+" c:"+s.getColumn(s.getStart()));
		}
		
		System.out.println("_________");
		
		// Call function that inserts needed INDENT and DEDENT tokens
		editTokenStream(scanner);
	}
	
	public void editTokenStream(LangScanner scanner){
		int i = 0;
		boolean indentable = false;
		Deque<Integer> indentStack = new ArrayDeque<Integer>();
		
		while (i < ts.size()) {
			// if IF is seen, set indentable to true and put the column of
			// IF on indentStack
			if (ts.get(i).getId() == Terminals.IF) {
				indentable = true;
				indentStack.addLast(col(ts,i));
                
            } else if (ts.get(i).getId() == Terminals.ELSE) {
                ts.add(i-1, scanner.sym(Terminals.DEDENT));
                indentStack.removeLast();
                indentable = true;
				indentStack.addLast(col(ts,i));
                i++;
			
			// If a NEWLINE is seen after IF (thus, indentable is true) 
			// insert INDENT token and set indentable to false
			} else if (ts.get(i).getId() == Terminals.NEWLINE && indentable) {
				// Add INDENT after current token (so at i+1)
				ts.add(i+1, scanner.sym(Terminals.INDENT));
				indentable = false;
			
			// don't do anything in case of NEWLINE while indentable is false
			} else if (ts.get(i).getId() == Terminals.NEWLINE) {
			
			// should EOF be reached and the stack of indents is not empty,
			// add as many DEDENTS as there are stack-elements
			} else if (ts.get(i).getId() == Terminals.EOF) {
				while (indentStack.size() > Terminals.EOF) {
					ts.add(i, scanner.sym(Terminals.DEDENT));
					indentStack.removeLast();
				}
			
			// In all other cases check whether following tokens are more indented 
			// than current indentStack, if they are less indented, insert DEDENT-token
			// pop last element off the stack and compare again...
			} else {
				while (indentStack.size() > Terminals.EOF && indentStack.getLast() >= col(ts,i)) {
                        //System.out.println("Stackvalue: "+indentStack.getLast());
                        //System.out.println("Colvalue: "+col(ts,i)+" at "+NAMES[ts.get(i).getId()]);
					// Add DEDENT before NEWLINE that was before current token (so at i-1)
					// This seems super HACKY!!! One can argue, that relative indentation 
					// can really only change after a newline... nonetheless... rethink!
					ts.add(i-1, scanner.sym(Terminals.DEDENT));
                    // apparently i has to be decremented next because of treatment of 
                    // ArrayList
                    i--;
					indentStack.removeLast();
				}
			}
			i++;
		}
		
		for(beaver.Symbol s: ts) {
			System.out.println("|"+Terminals.NAMES[s.getId()]+"| - l:"+s.getLine(s.getStart())+" c:"+s.getColumn(s.getStart()));
		}
	}

	// helper for easier access to column of tokenStream
	public int col(ArrayList<beaver.Symbol> tokenStream, int index) {
		return tokenStream.get(index).getColumn(tokenStream.get(index).getStart());
	}
	
	public beaver.Symbol nextToken() throws java.io.IOException {
		// simple function that beaver uses to get the next token
		counter++;
		return ts.get(counter-1);
	}	
}
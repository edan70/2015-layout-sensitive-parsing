package lang.ast;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol 
%function nextToken 

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  public beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

// macros
WhiteSpace = [ ] | \t | \f | \r
ID = [a-zA-Z]+
Numeral = [0-9]+
Newline = \n

%%

// discard whitespace information
{WhiteSpace}  { }

// token definitions

"if"            { return sym(Terminals.IF); }
"else"          { return sym(Terminals.ELSE); }
"elif"          { return sym(Terminals.ELIF); }
"=="            { return sym(Terminals.EQ); }
"="             { return sym(Terminals.ASSIGN); }
":"             { return sym(Terminals.COLON); }
{Newline}		{ return sym(Terminals.NEWLINE); }
{ID}            { return sym(Terminals.ID); }
{Numeral}       { return sym(Terminals.NUMERAL); }
<<EOF>>         { return sym(Terminals.EOF); }

// error fallback
[^]           { throw new SyntaxError("Illegal character <"+yytext()+">"); }

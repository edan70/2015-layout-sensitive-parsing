package lang.ast;

import lang.ast.LangParser.Terminals;
import lang.ast.LangParser.SyntaxError;
import lang.LPSymbol;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the preprocessor is the nextToken() method
%type LPSymbol 
%function nextToken 

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  public LPSymbol sym(short id) {
    return new LPSymbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

// macros
WhiteSpace = [ ] | \f | \r | \n
ID = [a-zA-Z]+
Numeral = [0-9]+

%%

// discard whitespace information
{WhiteSpace}  { }

// token definitions

"SEQ"           { return sym(Terminals.SEQ); }
"FOR"			{ return sym(Terminals.FOR); }
"="             { return sym(Terminals.EQ); }
":="            { return sym(Terminals.ASSIGN); }
"+"             { return sym(Terminals.PLUS); }
"-"				{ return sym(Terminals.MINUS); }
","				{ return sym(Terminals.COMMA); }
"("				{ return sym(Terminals.LPAR); }
")"				{ return sym(Terminals.RPAR); }
"\t"			{ return sym(Terminals.TAB); }
{ID}            { return sym(Terminals.ID); }
{Numeral}       { return sym(Terminals.NUMERAL); }
<<EOF>>         { return sym(Terminals.EOF); }

// error fallback
[^]           { throw new SyntaxError("Illegal character <"+yytext()+">"); }
